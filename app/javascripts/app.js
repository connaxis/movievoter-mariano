var accounts;
var account;
var allMovies;

function setStatus(message) {
  var status = document.getElementById("status");
  status.innerHTML = message;
}

function asyncGetMovies(scifi, i) {
  return scifi.movies(i).then(function(hexname) {
    return scifi.bids(hexname).then(function(value) {
      var ethValue = web3.fromWei(value.toNumber());
      return { name: web3.toAscii(hexname), bid: parseFloat(ethValue)};
    });
  })
};

function showMovies(movies) {
  var moviesel = document.getElementById("movies");
  moviesel.innerHTML = '';

  for (var i = 0; i < movies.length; i++) {
    moviesel.innerHTML += "<p>" + movies[i].name + " - " + movies[i].bid + " ETH</p>";
  }
}

function getAllMovies() {
  var scifi = SciFi.deployed();

  var movies = document.getElementById("movies");
  movies.innerHTML = '';

  allMovies = [];

  var promises = [];

  return scifi.movie_num().then(function(num) {
    for (var i = 0; i < num; i++) {
      promises.push(asyncGetMovies(scifi, i));
    }
    return promises;
  }).then(function(p) {
    return Promise.all(p);
  });
}

function mySort(a, b) {
  return (a.bid > b.bid) ? -1 : (( a.bid < b.bid) ? 1 : 0);
}

function setEvents() {
  var scifi = SciFi.deployed();

  var event = scifi.Voted();
  event.watch(function (error, result) { 
    if (error) {
      console.log("Error: " + error);
    } else {
      console.log(result);
      var movie = web3.toAscii(result.args.movieName);
      setStatus('ZOMG!! Someone just voted for ' + movie);
      getAllMovies().then((res) => {
        var sorted_array = res.sort(mySort);
        console.log(sorted_array);
        showMovies(sorted_array);
      });
    }
  });
}

function vote() {
  var scifi = SciFi.deployed();

  var defaultgas = 200000;

  var amount = document.getElementById("amount").value;
  var amount_in_wei = web3.toWei(amount, 'ether');

  var movie = document.getElementById("movie").value;

  setStatus("Voting for " + movie + " with " + amount + "... please wait");

  scifi.vote(web3.toHex(movie), {from: account, value: amount_in_wei, gas: defaultgas}).then(function(res) {
    setStatus('Success!!!!');
    //console.log(res);
  }).catch(function(error) {
    setStatus(error);
  });
}

window.onload = function() {
  web3.eth.getAccounts(function(err, accs) {
    if (err != null) {
      alert("There was an error fetching your accounts.");
      return;
    }

    if (accs.length == 0) {
      alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
      return;
    }

    accounts = accs;
    account = accounts[0];

    getAllMovies().then((res) => {
      var sorted_array = res.sort(mySort);
      console.log(sorted_array);
      showMovies(sorted_array);
    });
    setEvents();
  });
}
