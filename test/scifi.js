contract('SciFi', function(accounts) {
  it("should assert true", function() {
    var scifi = SciFi.deployed();
    assert.isTrue(true);
  });

  it("should assert false", function() {
    var scifi = SciFi.deployed();
    scifi.movies().then(function(res) {
      console.log(res);
      assert.isFalse(false);
    });
  });

  it("should vote for Matrix", function() {
    var scifi = SciFi.deployed();
    var movie = 'Matrix';
    
    var account_one = accounts[0];

    var vote1 = 500000;
    var vote2 = 250000;

    var vote_result = vote1 + vote2;
    
    return scifi.vote(web3.toHex(movie), {from: account_one, gas:200000, value: vote1}).then(function(res) {
      var num = scifi.movie_num().then(function(out) {
        assert.equal(out.toNumber(), 1, "Number is not 1");
      });

      scifi.vote(web3.toHex(movie), {from: account_one, gass: 200000, value: vote2}).then(function() {
        scifi.getVotesByMovie.call(movie).then(function(out) {
          assert.equal(out.toNumber(), vote_result, "Voting result is not equal");
        });
      });
    });
  });

  it("should vote for Predator", function() {
    var scifi = SciFi.deployed();
    var movie = 'Predator';
    
    var account_one = accounts[0];
    
    return scifi.vote(web3.toHex(movie), {from: account_one, gas:200000, value: 100000000}).then(function(res) {
      return scifi.movie_num();
    }).then(function(out) {
      assert.equal(out.toNumber(), 2, "Number is not 2");
    });;
  });

});
