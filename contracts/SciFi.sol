contract SciFi {

    mapping(bytes32 => uint) public bids;

    bytes32[1000000] public movies;

    uint public movie_num;

    event Voted(address indexed _from, bytes32 movieName, uint256 _value);

    function vote(bytes32 movieName) {
        if (msg.value == 0)
            return;

        uint val = bids[movieName];

        if (val == 0) {
            movies[movie_num++] = movieName;
        }

        bids[movieName] += msg.value;

        Voted(msg.sender, movieName, msg.value);
    }

    function getVotesByMovie(bytes32 movieName) returns(uint) {
        return bids[movieName];
    }
}